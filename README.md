Line Counting Standard
-Count comment
-Not count doc
Tools
-pygount

command is
pygount --format=summary readWriteFile.py

Language   Files    %     Code    %     Comment    %   
---------  -----  ------  ----  ------  -------  ------
Python         1  100.00   100  100.00       12  100.00
---------  -----  ------  ----  ------  -------  ------
Sum total      1           100               12        

#Modify : 2021-05-21
#Modify Description : -Add Error system check when type to enter function
#                     -Add system resume or end program when typing Error
#                     -fix check lenght of Data before Write to File
#Modify by : Phithak Buathong
