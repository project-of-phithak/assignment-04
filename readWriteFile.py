#Description of this Program
#Author : Phithak Buathong
#Since : 2021-05-06
#Program Name : Read & Write File
#Program Language : Python
#Program Purpose : Write a small program

#Modify : 2021-05-21
#Modify Description : -Add Error system check when type to enter function
#                     -Add system resume or end program when typing Error
#                     -fix check lenght of Data before Write to File
#Modify by : Phithak Buathong

def ReadFile(str):                      #function for Read File
    with open("file.txt",'r') as File:  #this function for open file
        DataInFile = File.read()        #read file and put to DataInFile
        print(DataInFile)               #show data in this file
        File.close()

def AcceptData(str):    #function for Accept File
    pass                #Just pass because not do anything

def CheckFloat(checkfloat): #funtion for Check is dot
    try:
        float(checkfloat)
        return True
    except ValueError:
        return False
    
def ErrorReplaceCheck(str): #funtion for check Error from funtion ReplaceData
        RESUME_PROGRAME = str(input("\nDo you wan't to Replace Data Again or End Program?\n\
If Replace Data Again  : rep\n\
If End Program Type    : end\n\
Type Here              : "))
        if RESUME_PROGRAME == "rep":     #this function for compare if get rep from keyboard then run ReplaceData Function
            ReplaceData(str)
        elif RESUME_PROGRAME == "end":   #this function for compare if get end from keyboard then End Program
            pass
        else:                            #this funtion for check typing is correct
            print("!!! ERROR !!! \nPlease Type rep or end")
            ErrorReplaceCheck(str)
    
def ReplaceData(str):                                       #function for Replace Data in File
    total = int(input("How Many to Write ? "))              #set total for get data from Keyboard
    Data = []
    CountData = 0
    for amount in range(total):                             #this function for read data in total
        insertData = input("Please Insert Data : ")         #this Variable for get data from Keyboard
        if insertData.isnumeric():
            Data.append(insertData)                         #this command for put data to Data Variable
            CountData += 1
        else:                                               #this funtion for check typing is correct
            if CheckFloat(insertData) is True:
                Data.append(insertData)
                CountData += 1
            else:                                           #this funtion for check typing is correct
                CountData -= 1
                print("!!! ERROR !!! \nPlease Type Number or dot")
                ErrorReplaceCheck(str)
                
    if len(Data) == CountData:                                  #this function for check comfirm to write file
        insertFileName = input("Please Insert File Name : ")    #this Variable for get data from Keyboard
        with open(insertFileName+".txt",'w') as File:           #this function for open file
            for element in Data:                                #this function for get data from Data Variable
                File.write(element + "\n")                      #this command for Write data from Data Variable to File
            File.close()

def DeleteData(str):                    #function for Delete Data in File
    with open("file.txt",'w') as File:  #this function for open file
        File.write("")                  #write space to File for delete Data
        File.close()

def ErrorAddCheck(str):
        RESUME_PROGRAME = str(input("\nDo you wan't to Add Data Again or End Program?\n\
If Add Data Again      : add\n\
If End Program Type    : end\n\
Type Here              : "))
        if RESUME_PROGRAME == "add":    #this function for compare if get add from keyboard then run AddData Function
            AddData(str)
        elif RESUME_PROGRAME == "end":  #this function for compare if get end from keyboard then run End Program
            pass
        else:                           #this funtion for check typing is correct
            print("!!! ERROR !!! \nPlease Type add or end")
            ErrorAddCheck(str)

def AddData(str):                                   #function for Add Data to File
    total = int(input("How Many to Add ? "))        #set total for get data from Keyboard
    Data = []
    CountData = 0
    for amount in range(total):                     #this function for read data in total
        insertData = input("Please Insert Data : ") #this Variable for get data from Keyboard
        if insertData.isnumeric():
            Data.append(insertData)                 #this command for put data to Data Variable
            CountData += 1
        else:                                       #this funtion for check typing is correct
            if CheckFloat(insertData) is True:
                Data.append(insertData)
                CountData += 1
            else:                                   #this funtion for check typing is correct
                CountData -= 1
                print("!!! ERROR !!! \nPlease Type Number or dot")
                ErrorAddCheck(str)
                
    if len(Data) == CountData:                          #this function for check comfirm to write file
        with open("file.txt",'a') as File:              #this function for open file
            for element in Data:                        #this function for get data from Data Variable
                File.write(element + "\n")              #this command for Add data from Data Variable to File
            File.close()
        
ReadFile(str)   #Rus function ReadFil for show Data in File

def SelectCheck(str):   #this funtion for check typing is correct
    SELECT = str(input("Accept Data Type  : acc \n\
Replace Data Type : rep \n\
Delete Data Type  : del \n\
Add Data Type     : add\n\
Type Here         : ")) #this Variable for get data from Keyboard

    if SELECT == 'acc':     #this function for compare if get acc from keyboard then run AcceptData Function
        AcceptData(str)
    elif SELECT == 'rep':   #this function for compare if get rep from keyboard then run ReplaceData Function
        ReplaceData(str)
    elif SELECT == 'del':   #this function for compare if get del from keyboard then run DeleteData Function
        DeleteData(str)
    elif SELECT == 'add':   #this function for compare if get add from keyboard then run AddData Function
        AddData(str)
    else:                   #this funtion for check typing is correct
        print("\n!!! ERROR !!!\nPlease Type acc , rep , del or add")
        ErrorSelectCheck(str)
        
def ErrorSelectCheck(str):  #funtion for check Error from funtion SelectCheck
        RESUME_PROGRAME = str(input("\nDo you wan't to Resume Program or End Program?\n\
If Resume Program Type : res\n\
If End Program Type    : end\n\
Type Here              : "))
        if RESUME_PROGRAME == "res":        #this function for compare if get add from keyboard then run SelectCheck Function
            SelectCheck(str)
        elif RESUME_PROGRAME == "end":      #this function for compare if get add from keyboard then run End Program
            pass
        else:                               #this funtion for check typing is correct
            print("!!! ERROR !!! \nPlease Type res or end")
            ErrorSelectCheck(str)
            
SelectCheck(str)